import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
public class App {
    public static void main(String[] args) throws Exception {
        try {
            FileReader fr = new FileReader("external/artikelliste.csv");
            BufferedReader reader = new BufferedReader(fr);

            String zeile;
            zeile = reader.readLine();

            if (zeile == ";") {
                zeile = reader.readLine();  
            }

            while(zeile != null) {
                System.out.println(zeile);
                zeile = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("There is an Error: " + e.toString());
        } catch (IOException e) {
            System.out.println("There is an Error: " + e.toString());
        }
    }
}
